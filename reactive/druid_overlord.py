import os, stat
from charms.reactive import when, when_not, when_any, is_flag_set, set_flag, clear_flag, endpoint_from_flag
from charms.templating.jinja2 import render
from charmhelpers.core.hookenv import open_port, resource_get, status_set, log
from subprocess import check_call


@when_not('druid-overlord.installed')
def install_druid_overlord():
    archive = resource_get("druid")
    if not os.path.isdir('/opt/druid_overlord'):
        os.mkdir('/opt/druid_overlord')
    cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_overlord', '--strip', '1']
    check_call(cmd)

    archive = resource_get("mysql-extension")
    cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_overlord/extensions']
    check_call(cmd)

    render('druid_overlord', '/etc/init.d/druid_overlord')

    render('druid_logrotate', '/etc/logrotate.d/druid_logrotate')
    st = os.stat('/etc/init.d/druid_overlord')
    os.chmod('/etc/init.d/druid_overlord', st.st_mode | stat.S_IEXEC)

    open_port(8090)

    set_flag('druid-overlord.installed')
    status_set('waiting', 'Waiting for config file')

@when('druid-overlord.installed', 'endpoint.config.new_config')
def configure_druid_overlord():
    with open('/opt/druid_overlord/conf/druid/_common/common.runtime.properties', 'r') as c:
        old_conf = c.read()

    config = endpoint_from_flag('endpoint.config.new_config')
    new_conf = config.get_config()

    if old_conf != new_conf:
        if is_flag_set('druid-overlord.configured'):
            clear_flag('druid-overlord.configured')

        status_set('maintenance', 'Configuring Overlord')

        config_file = open('/opt/druid_overlord/conf/druid/_common/common.runtime.properties', 'w')
        config_file.write(new_conf)
        config_file.close()

        set_flag('druid-overlord.configured')
        set_flag('druid-overlord.new_config')
        status_set('maintenance', 'Druid Overlord configured, waiting to start process...')


@when('druid-overlord.installed', 'endpoint.config.new_hdfs_files')
@when_not('druid-overlord.hdfs_configured')
def configure_hdfs_files():
    hdfs = endpoint_from_flag('endpoint.config.new_hdfs_files')
    new_hdfs_files = hdfs.get_hadoop_files()

    status_set('maintenance', 'Copying HDFS XML Files.')

    with open('/opt/druid_overlord/conf/druid/_common/core-site.xml', 'w') as f:
        f.write(new_hdfs_files[0])
    with open('/opt/druid_overlord/conf/druid/_common/hdfs-site.xml', 'w') as f:
        f.write(new_hdfs_files[1])
    with open('/opt/druid_overlord/conf/druid/_common/mapred-site.xml', 'w') as f:
        f.write(new_hdfs_files[2])
    with open('/opt/druid_overlord/conf/druid/_common/yarn-site.xml', 'w') as f:
        f.write(new_hdfs_files[3])

    if is_flag_set('druid-overlord.configured'):
        clear_flag('druid-overlord.configured')

    set_flag('druid-overlord.hdfs_configured')
    set_flag('druid-overlord.new_config')
    status_set('waiting', 'HDFS files copied. Waiting...')

@when_any('druid-overlord.configured', 'druid-overlord.hdfs_configured')
@when('java.ready', 'druid-overlord.new_config')
def run_druid_overlord(java):
    restart_overlord()
    clear_flag('druid-overlord.new_config')

def start_overlord():
    status_set('maintenance', 'Starting Overlord...')
    cmd = ['/etc/init.d/druid_overlord', 'start']
    check_call(cmd)
    set_flag('druid-overlord.running')
    status_set('active', 'Overlord running')

def stop_overlord():
    cmd = ['/etc/init.d/druid_overlord', 'stop']
    check_call(cmd)
    clear_flag('druid-overlord.running')

def restart_overlord():
    status_set('maintenance', 'Restarting Overlord...')
    stop_overlord()
    start_overlord()

