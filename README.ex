# Overview

Welcome to the Druid Overlord charm by Spicule. The Druid Overlord node is
responsible for accepting tasks, coordinating task distribution, creating locks 
around tasks, and returning statuses to callers. It also provides a console which
can be used to view pending tasks, running tasks, available workers, and recent 
worker creation and termination.

For more information on Druid Overlord, please visit the [Druid Overlord Documentation](http://druid.io/docs/latest/design/overlord.html).

# Usage

To deploy this charm from the command line, enter the following:

    juju deploy cs:~spiculecharms/druid-overlord --series xenial

You can specify hardware constraints for your Overlord node on the command 
line. To find out more, refer to the ["Using Constraints"](https://docs.jujucharms.com/2.4/en/charms-constraints").
For example, to deploy your Overlord node on a server with 4 cores and 15 GB
of RAM, enter the following command:

    juju deploy cs:~spiculecharms/druid-overlord --series xenial --constraints "mem=15G cores=4"

# OpenJDK and Druid Config

Druid Overlord requires a relation to the [OpenJDK charm](https://jujucharms.com/openjdk/)
to automate the installation of Java onto Overlord's server, which is a
requirement of Overlord.

Additionally, Overlord requires configuration as part of the wider Druid
cluster, so Overlord must be related to our [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).
The individual Druid nodes do not need to be directly related to one another,
and should instead be directly related to Druid Config instead. 

# Exposing and Accessing the Console

Once Overlord has been installed and configured correctly, you can expose the 
Overlord node through Juju, which will enable you to access the web console.
To expose Overlord, enter the following command via the command line:

    juju expose druid-overlord

This will open the port for your Overlord node. Use the following command to
check the status of the server adjusting its firewall settings, as well as
getting the IP address of the server:

    juju status

Once the firewall has opened up port 8090 and you know your Overlord node's IP
address, you can use your favourite web browser to visit the web console:

    http://<overlord-ip-address>:8090

To unexpose your Overlord instance, simply enter the following command through
the command line:

    juju unexpose druid-overlord

# Configuration

Druid Cluster configuration is handled through the [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).

# Contact Information

Thank you for using the Druid Overlord charm - we hope you find it useful! For
more information, here are our contact details:

Spicule: [http://www.spicule.co.uk](http://www.spicule.co.uk)  
Anssr: [http://anssr.io](http://anssr.io)  
Email: info@spicule.co.uk  

For assistance with Juju, please refer to the [Juju Discourse](https://discourse.jujucharms.com/).

## Druid

For more information about Druid, please refer to the [Druid website](http://druid.io).

